import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Image,
  Platform,
  useEffect
} from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from "react-native-safe-area-context";
import AsyncStorage from '@react-native-community/async-storage';
import AppIntroSlider from 'react-native-app-intro-slider';
import SplashScreen from 'react-native-splash-screen'

import MainApp from './src/screens/MainApp/mainApp'
import Product from './src/screens/product/product'
import CategoryScreen from './src/screens/categoryScreen/categoryScreen';
import ShoppingCart from './src/screens/shoppingCart/shoppingCart';

import CartContext from './src/context/cart-context'

StatusBar.setBarStyle("light-content");
if (Platform.OS === "android") {
  StatusBar.setBackgroundColor("rgba(0,0,0,0)");
  StatusBar.setTranslucent(true);
}

const Stack = createStackNavigator();

const slides = [
  {
    key: '1',
    title: '\n\nWelcome to Mamapregi',
    text: 'The BIGGEST online\npregnancy market!!',
    image: require('./src/assets/logo_icons/logoIcons.png'),
    backgroundColor: '#ef0b6d',
  },
  {
    key: '2',
    title: ' ',
    text: 'Get all your items\nonline at cheap prices',
    image: require('./src/assets/2.png'),
    backgroundColor: '#ef0b6d',
  },
  {
    key: '3',
    title: '',
    text: 'Discounts are available!!',
    image: require('./src/assets/3.png'),
    backgroundColor: '#ef0b6d',
  },
  {
    key: '4',
    title: '\n\nFree Delivery',
    text: 'For all orders over Gh₵ 500',
    image: require('./src/assets/rocket-sharp.png'),
    backgroundColor: '#ef0b6d',
  }
];


const headerTint = Platform.OS === 'ios' ? 'orange' : 'white';
const headerStyling = { backgroundColor: Platform.OS === 'android' ? 'orange' : 'white' };
export default class App extends React.Component {
  state = {
    showRealApp: false,
    cartItems: 0
  }

  componentDidMount() {
    SplashScreen.hide();
    AsyncStorage.getItem("@showIntro", (err, result) => {

      if (err) {
      } else {
        if (result == null) {
          this._showApp(false);
        } else {
          this._showApp(true);
        }
      }


    });

    this.getCartItems()
    //   this.focusListener = this.props.navigation.addListener("focus", () => {
    //     this.getCartItems()
    //  });
  }
  // componentWillUnmount() {
  //   this.getCartItems();
  // }

  async getCartItems() {
    await AsyncStorage.getItem("@Cart", (err, result) => {
      if (err) {
      } else {
        if (result == null) {
          // console.log(JSON.parse(result));
          // this.setState({ cartItems: 0 })
        } else {
          // console.log('[App.js] Context RUN');
          this.setState({ cartItems: JSON.parse(result).value.length })
        }
      }
    });
  }




  _renderItem = ({ item }) => {
    return (
      <View style={{
        flex: 1,
        backgroundColor: item.backgroundColor,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingBottom: 100
      }}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  }


  _showApp(visible) {
    if (visible === true) {
      AsyncStorage.setItem("@showIntro", JSON.stringify({ "value": "true" }), (err, result) => {
        // console.log("error", err, "result", result);
      })
    }
    this.setState({ showRealApp: visible });

  }

  render() {
    if (this.state.showRealApp === true) {
      return (
        <CartContext.Provider value={{ cartItems: this.state.cartItems, getCartItems: () => this.getCartItems() }}>
          <NavigationContainer>
            <SafeAreaProvider>
              <Stack.Navigator>
                <Stack.Screen
                  name="Home"
                  component={MainApp}
                  options={{ headerShown: false }} />
                <Stack.Screen
                  name="Product"
                  component={Product}
                  options={
                    {
                      headerTitle: 'Product',
                      headerTintColor: headerTint,
                      headerStyle: headerStyling,
                      headerShown: false
                    }} />
                <Stack.Screen
                  name="CategoryScreen"
                  key='CategoryScreen'
                  component={CategoryScreen}
                  options={
                    {
                      headerTitle: 'Category Item',
                      headerTintColor: headerTint,
                      headerStyle: headerStyling
                    }} />
                <Stack.Screen
                  name="ShoppingCart"
                  component={ShoppingCart}
                  options={{
                    headerTitle: 'Shopping Cart',
                    headerTintColor: headerTint,
                    headerStyle: headerStyling
                  }} />
              </Stack.Navigator>
            </SafeAreaProvider>
          </NavigationContainer>
        </CartContext.Provider>
      );
    } else {
      return (<AppIntroSlider renderItem={this._renderItem} data={slides} onDone={() => this._showApp(true)} />);
    }
  }
}


const styles = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
  },
  text: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    paddingVertical: 30,
  },
  title: {
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
    marginBottom: 16,
  }
});