import React from "react";
import { View } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

const BottomMenuItem = ({ iconName, isCurrent }) => {
  return (
    <View
      style={{
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Icon
        name={iconName}
        size={32}
        style={[
          { color: isCurrent ? 'orange' : 'grey' },
          // {color:iconName=='question-circle'?'#a35374':'grey'}
        ]}
      />
    </View>
  );
};
export default BottomMenuItem




