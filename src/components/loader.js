import React from 'react'
import { View, Text ,ActivityIndicator} from 'react-native'

const Loader = () => {
    return (
        <View style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}>
            <ActivityIndicator size='large' animating  color="orange"/>
            </View>
    )
}

export default Loader
