import React, { Component, useRef, useEffect } from 'react'
import { Text, View, Animated, Dimensions,Keyboard } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const { width,height } = Dimensions.get('window');

const Modal = (props) => {
    const top = useRef(new Animated.Value(height)).current;

    const toggleModal = () => {
        Animated.spring(top, {
            toValue: 0,
            useNativeDriver: true
        }).start()
    }
    const closeModal = () => {
        Animated.spring(top, {
            toValue: height,
            useNativeDriver: true
        }).start()
        Keyboard.dismiss()
    }
    if (props.modal === true) {
        toggleModal()
    }
    if (props.modalC === false) {
        closeModal()
    }

    return (
        <Animated.View style={{
            backgroundColor: 'white',
            width: '100%',
            height: height,
            zIndex: 100,
            transform: [{
                translateY: top
            }],
            position: 'absolute',
            alignItems: 'center',
            borderRadius: 30,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.39,
            shadowRadius: 8.30,
            elevation: 13,
            paddingTop: 20,
            zIndex: 9

        }}>
            {/* <View onTouchEnd><Text> chevron-down </Text></View> */}
            <TouchableOpacity hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }} style={{ marginBottom: 14 }}><FontAwesome5 onPress={closeModal} name="times" color="#ccc" size={20} /></TouchableOpacity>

            {/* <ScrollView contentContainerStyle={{flex:1 ,width:width*0.8}}> */}

            {props.children}
            {/* </ScrollView> */}
        </Animated.View>
    )

}
export default Modal