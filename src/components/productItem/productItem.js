import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native'

const ProductItem = (props) => {
    // const image = require({uri:props.itemData.item.images[0].thumbnail})
    // console.log(props.itemData.item.images[0].thumbnail);

    let price = props.itemData.prices.price;
    price /= Math.pow(10, 2);
    return (<>
        <ImageBackground imageStyle={{ borderRadius: 8}} source={{ uri: props.itemData.images[0].thumbnail }} style={{ width: 100, height: 100}}><View style={[styles.itemStatus, { backgroundColor: props.itemData.is_in_stock ? 'green' : 'black' }]}><Text style={{ color: 'white' }}>{props.itemData.is_in_stock ? 'In stock' : 'Out of stock'}</Text></View></ImageBackground>
        <Text style={styles.itemName}>{props.itemData.name}</Text>
        <Text style={styles.price}>{`GH₵ ${price}.00`}</Text>
    </>

    )
}

const styles = StyleSheet.create({
    itemStatus: { backgroundColor: 'green', alignSelf: 'flex-end', padding: 3 ,borderTopRightRadius:8},
    price: { fontSize: 14 },
    itemName: { color: '#005296', fontSize: 12,width:100 }
})

export default ProductItem
