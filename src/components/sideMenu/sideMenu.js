import React, { Component, useRef, useEffect } from 'react'
import { Text, View, Animated, Dimensions, Image, TouchableOpacity, StyleSheet, Linking, SafeAreaView } from 'react-native'
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get('window');
const logo = require('../../assets/logo_icons/logoSlide.png')



const categories = [
    { key: '0', slugTitle: 'mamapregi-hospital-bag', title: 'Mamapregi Hospital Bag' ,id:178},
    { key: '1', slugTitle: 'akwaaba-gift-box', title: 'Akwaaba Gift Box' ,id:305},
    { key: '2', slugTitle: 'baby-footwear', title: 'Baby Footwear' ,id:131},
    { key: '3', slugTitle: 'feeding', title: 'Feeding',id:104 },
    { key: '4', slugTitle: 'baby-hair-accessories', title: 'Baby Hair Accessories' ,id:120},
    { key: '5', slugTitle: 'health-beauty', title: 'Health & Beauty' ,id:170},
    { key: '6', slugTitle: 'toys-learning', title: 'Toys & Learning',id:180 },
    { key: '7', slugTitle: 'baby-clothing', title: 'Baby Clothing',id:176 },
    { key: '8', slugTitle: 'baby-changing-nappies', title: 'Baby Changing & Nappies',id:189 },
    { key: '9', slugTitle: 'toddler-swimming-Accessories', title: 'Toddler Swimming Accessories',id:192 },
    { key: '10', slugTitle: 'detergents', title: 'Detergents' ,id:184},
    { key: '11', slugTitle: 'baby-care', title: 'Baby Care' ,id:190},
    { key: '12', slugTitle: 'maternity-wear', title: 'Maternity Wear',id:191 }]


const SideMenu = (props) => {
    const side = useRef(new Animated.Value(width * -1)).current

    const toggleModal = () => {
        Animated.spring(side, {
            toValue: width / -2.4,
            useNativeDriver: true
        }).start()
    }
    const closeModal = () => {
        Animated.spring(side, {
            toValue: width * -1,
            useNativeDriver: true
        }).start()
    }
    // console.log(props.modal);
    if (props.show === true) {
        toggleModal()
    }


    const openPage = (data) => {
        props.nav.navigate('CategoryScreen',data)
        closeModal()
    }


    return (
        <Animated.View style={{
            backgroundColor: 'white',
            width: '100%',
            height: '100%',
            zIndex: 200,
            alignItems: 'flex-end',
            transform: [{
                translateX: side,
            }],
            // marginLeft: side,
            position: 'absolute',
            // alignItems: 'center',
            borderRadius: 30,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.39,
            shadowRadius: 8.30,
            elevation: 13,
            padding: 20,
            paddingLeft:0,
            overflow: 'hidden',
            paddingTop: 40
        }}>
            <TouchableOpacity style={{alignSelf: 'flex-end' }} onPress={closeModal} hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}>
                <Icon name="times" color="#ccc" size={20} />
            </TouchableOpacity>
            <View style={{ width: '58%',height:"73%", padding: 20, alignSelf: 'flex-end' }}>
                {/* {categoriesView} */}
                <FlatList
                    data={categories}
                    ListHeaderComponent={<Text style={{color:'#d17098',marginBottom:10,fontWeight:'bold'}}>Categories</Text>}
                    renderItem={(itemData) => {
                        let item = itemData.item;
                        return <TouchableOpacity onPress={() => { openPage({categoryID: item.title, serverID: item.id}) }} style={styles.btn}>
                            <Text style={styles.menuBtn}>{item.title}</Text>
                        </TouchableOpacity>
                    }}
                />
            </View>


            <View style={{
                position: "absolute",
                height: '30%',
                bottom: 0,
                // alignSelf:'flex-end',
                padding: 20, width: '63%', backgroundColor: 'orange'
            }}>
                <TouchableOpacity onPress={() => { Linking.openURL('https://mamapregimarket.com/about-us/') }}><Text style={[styles.menuBtn, { color: 'white', marginBottom: 18, }]}>About Us</Text></TouchableOpacity>
                <TouchableOpacity onPress={() => { Linking.openURL('https://mamapregimarket.com/contact/') }}><Text style={[styles.menuBtn, { color: 'white', marginBottom: 18, }]}>Contact</Text></TouchableOpacity>
                <View style={{ flexDirection: 'row', alignItems: 'center' }} onTouchEnd={() => { Linking.openURL('https://mamapregimarket.com/') }}>
                    <Image source={logo} style={{ width: 60, height: 60 }} />
                </View>
            </View>
            {/* </View> */}
        </Animated.View>
    )

}

const styles = StyleSheet.create({
    menuBtn: {
        fontSize: 15,
        color: 'orange',
        fontWeight: "600",
        width: '100%'
    }, btn: {
        alignItems: 'center',
        flexDirection: 'row',
        // justifyContent: 'space-between',
        // borderWidth: 2,
        marginBottom: 5,
        width: '100%',
        borderRadius:12,
        backgroundColor:'#f9f9f9',
        padding:10
    }
})
export default SideMenu