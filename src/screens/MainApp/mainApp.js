import React, { useEffect, useState } from 'react'
import { View, StyleSheet, TouchableOpacity, StatusBar, Text } from 'react-native'
import SideMenu from '../../components/sideMenu/sideMenu'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSafeArea } from "react-native-safe-area-context";
import Icon from 'react-native-vector-icons/Entypo';
import CartContext from '../../context/cart-context';
import TabBar from "../../components/tabbar";

import Home from '../home/home'
import Shop from '../shop/shop'
import Search from '../search/search'
import Info from '../info/info'
import Favourites from '../favourites/favourites'
import { useIsFocused } from "@react-navigation/native";


const MainPage = (props) => {

    if (Platform.OS === "android") {
        StatusBar.setBarStyle("light-content");
        StatusBar.setBackgroundColor("orange");
        StatusBar.setTranslucent(true);
    }else{
        StatusBar.setBarStyle("dark-content")
    }
    const isFocused = useIsFocused();

   
    
    const Tab = createBottomTabNavigator();

    const [menuState, changeMenuState] = useState(false);

    const menuFunction = () => {
        changeMenuState(true);
        setTimeout(() => {
            changeMenuState(false);
        }, 100)
    }
    return (<>
        <SideMenu show={menuState} nav={props.navigation} />
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.topBtns}
                onPress={() => menuFunction()}>
                <Icon name={'menu'}
                    size={32}
                    style={{ color: 'orange' }}
                />
            </TouchableOpacity>

            <CartContext.Consumer>
                {context => <TouchableOpacity
                    style={[styles.topBtns, { alignSelf: 'flex-end', marginLeft: 0, right: 20, justifyContent: null }]}
                    onPress={() => {props.navigation.navigate('ShoppingCart')}}>
                    <View style={styles.badge}><Text style={{ color: 'white' }}>{context.cartItems}</Text></View>
                    <Icon name={'shopping-cart'}
                        size={25}
                        style={{ color: 'orange', alignSelf: 'center', marginTop: 12 }}
                    />
                </TouchableOpacity>}
            </CartContext.Consumer>

            <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
                <Tab.Screen name="Home" component={Home} options={{ title: 'home' }} />
                <Tab.Screen name="Shop" component={Shop} options={{ title: 'shopping-bag' }} />
                <Tab.Screen name="Search" component={Search} options={{ title: 'search' }} />
                <Tab.Screen name="Favourites" component={Favourites} options={{ title: 'heart' }} />
                <Tab.Screen name="Info" component={Info} options={{ title: 'question-circle' }} />
            </Tab.Navigator>
            {useSafeArea().bottom > 0 && (<View
                style={{ height: useSafeArea().bottom - 5, backgroundColor: "white", }}
            />
            )}
        </View>
    </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // position: "relative"
        // paddingBottom: 50,
    },
    badge: {
        backgroundColor: 'red',
        width: 15,
        height: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        alignSelf: 'flex-start',

        position: 'absolute'
    },
    topBtns: {
        zIndex: 99,
        position: 'absolute',
        marginLeft: 20,
        marginTop: Platform.OS === 'android'? 30:50,
        backgroundColor: 'white',
        borderRadius: 25,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.7,
    }
})
export default MainPage