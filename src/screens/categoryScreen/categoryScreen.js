import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, ActivityIndicator, ImageBackground } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import AbortController from "abort-controller"

class CategoryScreen extends Component {
    _isMounted = false;
    state = {
        products: [],
        isloaded: false,
        page: 1,
        error: false
    }

    abortController = new AbortController();
    componentDidMount() {
        this._isMounted = true;
        this.fetchData();
    }
    componentWillUnmount() {
        this._isMounted = false;
        this.abortController.abort()
    }

    fetchData() {
        const { serverID } = this.props.route.params;
        fetch(`https://mamapregi-server.herokuapp.com/category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ request: `products?category=${serverID}&&?page=${this.state.page}` }),
            // body: JSON.stringify({request: `products?category=170&&?page=1`}),
            signal: this.abortController.signal
        })
            .then((res) => res.json())
            .then((res) => {
                if (this._isMounted) {
                    this.setState((state) => ({ products: [...state.products, ...res], isloaded: true }))
                }
            })
            .catch((err) => {
                console.log(err);
                this.setState({ error: true });
            })

    }

    render() {
        let { categoryID } = this.props.route.params;
        return (
            <View style={styles.container}>
                <Text style={styles.headerText}> {categoryID} </Text>
                {/* PRODUCTS */}
                <FlatList
                    data={this.state.products}
                    keyExtractor={(x, i) => i}
                    numColumns={2}
                    onEndReached={() => {
                        this.setState((state) => ({ page: state.page + 1 }), () => { this.fetchData() })
                    }}
                    onEndReachedThreshold={5}
                    ListFooterComponent={<ActivityIndicator size="large" color="grey" animating={!this.state.isloaded} style={{ marginTop: 200 }} />}
                    renderItem={itemData =>
                        <TouchableOpacity containerStyle={styles.gridItem} onPress={() => {
                            this.props.navigation.navigate('Product', {
                                product: itemData.item,
                                previous_screen: 'categoryScreen'
                            })
                        }}>
                            <ImageBackground imageStyle={{ borderRadius: 8 ,backgroundColor:'#e5e5e5'}} source={{ uri: itemData.item.images[0].src }} style={{ width: 100, height: 100, borderTopRightRadius: 8 }}><View style={[styles.itemStatus, { backgroundColor: itemData.item.stock_status === 'instock' ? 'green' : 'black' }]}><Text style={{ color: 'white' }}>{itemData.item.stock_status === 'instock' ? 'In stock' : 'Out of stock'}</Text></View></ImageBackground>
                            <Text style={styles.itemName}>{itemData.item.name}</Text>
                            <Text style={styles.price}>{`GH₵ ${itemData.item.price}`}</Text>
                        </TouchableOpacity>
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    itemStatus: { position: 'absolute', backgroundColor: 'green', alignSelf: 'flex-end', padding: 3, borderTopRightRadius: 8 },
    price: { fontSize: 14 },
    itemName: { color: '#005296', fontSize: 12, width: 100 },
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 18,
        // fontFamily:
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        color: 'black',
        fontWeight: '700'
    },
    // itemStatus: {  backgroundColor: 'green', alignSelf: 'flex-end' },
    gridItem: { flex: 1, margin: 15, alignItems: 'center' }
})

export default CategoryScreen;