import React, { useEffect, useState } from 'react'
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    ImageBackground,
    TouchableOpacity
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { useIsFocused } from "@react-navigation/native";


const Favourite = (props) => {
    // console.log(props.itemData);
    let price = props.itemData._price;
    // price /= Math.pow(10, 2);
    return (<>
        <View>
            <ImageBackground source={{ uri: props.itemData.images[0].thumbnail || props.itemData.images[0].src }} style={{ width: 100, height: 100 }}>
                <View style={[styles.itemStatus, { backgroundColor: props.itemData.is_in_stock || props.itemData.stock_status ? 'green' : 'black' }]}>
                    <Text style={{ color: 'white' }}>{props.itemData.is_in_stock || props.itemData.stock_status ? 'In stock' : 'Out of stock'}</Text>
                </View>
            </ImageBackground>
            <Text style={styles.itemName}>{props.itemData.name}</Text>
            <Text style={styles.price}>{`GH₵ ${price}`}</Text>
        </View>
    </>

    )
}
const Favourites = (props) => {

    const [favouriteItems, setFavouritesState] = useState([])

    const isFocused = useIsFocused();

    useEffect(() => {

        getFavourites()

    }, [])
    useEffect(() => {

        getFavourites()

    }, [isFocused])

    const getFavourites = async () => {
        await AsyncStorage.getItem("@favourites", (err, result) => {
            if (err) {
                console.log(err);
            } else {
                if (result == null) {
                    // console.log("null value recieved", result);
                } else if (result === undefined) {
                    // console.log("EMPTY:", result);
                    setFavouritesState([])
                } else {
                    // console.log("FAVOURITIES:", result);
                    setFavouritesState(JSON.parse(result).value)
                }
            }
        });
    }
    // {favouriteItems==[]?<Text>No favourites</Text>:null}

    if (favouriteItems.length < 1) {
        return <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}><Text style={{ color: 'orange' }}>No favourites</Text></View>
    }
    return (
        <View style={styles.container}>
            <FlatList
                data={favouriteItems}
                keyExtractor={(x, i) => i}
                numColumns={2}
                renderItem={itemData => {
                    // console.log(itemData);
                    return (
                        <TouchableOpacity style={styles.gridItem} onPress={() => {
                            props.navigation.navigate('Product', { product: itemData.item,previous_screen:'Favourites'})
                        }}>
                            <Favourite itemData={itemData.item} />
                        </TouchableOpacity>
                    )
                }
                }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    itemStatus: { backgroundColor: 'green', alignSelf: 'flex-end', padding: 3 },
    favourite: {
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 25,
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        paddingVertical: 67,
        // justifyContent: 'space-eve',
        // backgroundColor:'red'
    },
    price: { fontSize: 14 },
    itemName: { color: '#005296', fontSize: 12, width: 100 },
    gridItem: { flex: 1, margin: 15, alignItems: 'center' },

    // img: {
    //     width: 130,
    //     height: 130
    // }
})
export default Favourites
