import React, { Component, PureComponent } from 'react'
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    ActivityIndicator,
    Platform
} from 'react-native'
import { FlatListSlider } from 'react-native-flatlist-slider';
import SliderItem from '../../components/sliderItem';

const { width, height } = Dimensions.get('window');
class Home extends Component {
    state = {
        products: [],
        isloaded: false,
        page: 1,
        error: false,
        sliderInfo: [
            {
                image: 'https://mamapregimarket.com/wp-content/uploads/2019/08/hospital-bag1.jpg',
                desc: 'Mamapregi Hospital Bag',
                cart: 'Mamapregi Hospital Bag',
                key: "0"
            },
            {
                image: 'https://mamapregimarket.com/wp-content/uploads/2019/08/mamapregi-advert1.jpg',
                desc: '100% Quality',
                cart: 'Health & Beauty',
                key: "1"
            },
            {
                image: 'https://mamapregimarket.com/wp-content/uploads/2019/08/baby-towel.jpg',
                desc: 'Medium towels for babies',
                cart: 'Baby Care',
                key: "2"
            },
        ],
        data: [
            { loading: true, key: '1', serverID: 131, title: 'Baby Footwear', slugTitle: 'baby-footer', gridData: [] },
            { loading: true, key: '2', serverID: 176, title: 'Baby Clothing', slugTitle: 'baby-clothing', gridData: [] },
            { loading: true, key: '3', serverID: 120, title: 'Baby Hair Accessories', slugTitle: 'baby-hair-accessories', gridData: [] },
            { loading: true, key: '4', serverID: 184, title: 'Detergent', slugTitle: 'detergent', gridData: [] },
            { loading: true, key: '5', serverID: 170, title: 'Health & Beauty', slugTitle: 'health-beauty', gridData: [] }
        ]
    }

    componentDidMount() {
        this.getItems(0, 131)
        this.getItems(1, 176)
        this.getItems(2, 120)
        this.getItems(3, 184)
        this.getItems(4, 170)
    }

    async getItems(arrayIndex, ID) {
        await fetch(`https://mamapregi-server.herokuapp.com/category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ request: `products?category=${ID}` })
        })
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                const items = res.slice(0, 4)
                let data = [...this.state.data];
                // console.log(data);
                data[arrayIndex].gridData = items;
                data[arrayIndex].loading = false;
                this.setState({ data });
                // console.log(data);
            })
            .catch((err) => {
                console.log(err);
                this.setState({ error: true });
            })
    }

    render() {
        // this.context.getCartItems();
        const images = this.state.sliderInfo;

        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={(x, i) => x.key}
                    data={this.state.data}
                    ListHeaderComponent={() =>
                        <View style={{ width: '100%', marginBottom: 30, marginTop: 40 }}>
                            <FlatListSlider
                                data={images}
                                component={<SliderItem/>}
                                timer={5000}
                                key={images[0]}
                                indicatorActiveWidth={15}
                                onPress={(e) => { this.props.navigation.navigate('CategoryScreen', { categoryID: e.cart }) }}
                            />
                        </View>
                    }
                    // ListEmptyComponent = {<ActivityIndicator size="large" color="grey" animating={itemData.item.loading} style={{ marginTop: 200 }} />}
                    renderItem={itemData => {
                        if (itemData.item.loading) {
                            return (<ActivityIndicator size="large" color="grey" animating={itemData.item.loading} style={{ marginTop: 200 }} />)
                        } else {
                            return (<View style={{ width: width, alignItems: 'center' }} key={itemData.item.id}>
                                <View style={styles.categoryBox}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10 }}>
                                        <Text style={styles.categoryText}>{itemData.item.title}</Text>
                                        <TouchableOpacity onPress={() =>
                                            this.props.navigation.navigate('CategoryScreen', {
                                                categoryID: itemData.item.title,
                                                serverID: itemData.item.serverID
                                            })
                                        }><Text style={{ color: 'orange' }}>View more</Text></TouchableOpacity>
                                    </View>

                                    <View style={styles.row}>

                                        <TouchableOpacity containerStyle={styles.gridItem} onPress={() => {
                                            this.props.navigation.navigate('Product', {
                                                product: itemData.item.gridData[0],
                                                previous_screen: 'home'
                                            })
                                        }}>
                                            <ProductItem itemData={itemData.item.gridData[0]} />
                                        </TouchableOpacity>
                                        <TouchableOpacity containerStyle={styles.gridItem} onPress={() => {
                                            this.props.navigation.navigate('Product', {
                                                product: itemData.item.gridData[1],
                                                previous_screen: 'home'
                                            })
                                        }}>
                                            <ProductItem itemData={itemData.item.gridData[1]} />
                                        </TouchableOpacity>

                                    </View>
                                    <View style={styles.row}>
                                        <TouchableOpacity containerStyle={styles.gridItem} onPress={() => {
                                            this.props.navigation.navigate('Product', {
                                                product: itemData.item.gridData[2],
                                                previous_screen: 'home'
                                            })
                                        }}>
                                            <ProductItem itemData={itemData.item.gridData[2]} />
                                        </TouchableOpacity><TouchableOpacity containerStyle={styles.gridItem} onPress={() => {
                                            this.props.navigation.navigate('Product', {
                                                product: itemData.item.gridData[3],
                                                previous_screen: 'home'
                                            })
                                        }}>
                                            <ProductItem itemData={itemData.item.gridData[3]} />
                                        </TouchableOpacity>

                                    </View>
                                </View>
                            </View>)
                        }
                    }
                    }
                />

            </View>
        )
    }
}


const ProductItem = (props) => {
    return (<>
        <ImageBackground 
        imageStyle={{ borderRadius: 8,backgroundColor:'#e5e5e5' }} 
        source={{ uri: props.itemData.images[0].src }} 
        style={{ width: 100, height: 100 }}>
            <View style={[styles.itemStatus, 
            { backgroundColor: props.itemData.stock_status === 'instock' ? 'green' : 'black' }]}>
                <Text style={{ color: 'white' }}>{props.itemData.stock_status === 'instock' ? 'In stock' : 'Out of stock'}</Text>
                </View>
        </ImageBackground>
        <Text style={styles.itemName}>{props.itemData.name}</Text>
        <Text style={styles.price}>{`GH₵ ${props.itemData.price}`}</Text>
    </>
    )
}

const styles = StyleSheet.create({
    gridItem: { flex: 1, margin: 15 },
    itemStatus: { backgroundColor: 'green', alignSelf: 'flex-end', padding: 3, borderTopRightRadius: 8 },
    price: { fontSize: 14 },
    itemName: { color: '#005296', fontSize: 12, width: 100 },
    catergoryImage: {
        width: 100,
        height: 100,
        backgroundColor: 'grey',
        marginVertical: 10,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        paddingBottom: 62,
        paddingTop: Platform.OS === 'android'? 0:40
    },
    categoryText: {
        fontSize: 18,
        fontWeight: 'bold',
        width: '65%'
    },
    slider: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: width * 0.18,
        marginTop: height * 0.08,
    },
    categoryBox: {
        backgroundColor: '#f4f4f4',
        width: '80%',
        justifyContent: 'center',
        padding: 20,
        marginBottom: 20,
        borderRadius: 9,
        overflow: 'hidden'
    }
})
export default Home
