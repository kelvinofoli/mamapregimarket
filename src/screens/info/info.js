import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity,Linking } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const Info = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={[styles.btn,{padding:5,width:'30%',backgroundColor:'orange'}]}>
                <Text style={{ color: 'white', fontSize: 15 }}>Chat with us</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => { Linking.openURL('whatsapp://send?text=&phone=233504709463') }}>
                <Icon name='whatsapp' size={60} color="white" />
            </TouchableOpacity >
            <TouchableOpacity style={styles.btn} onPress={() => { Linking.openURL('https://m.me/mamapregimarket') }}>
                <Icon name='facebook' size={60} color="white" />
            </TouchableOpacity>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffc9df',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        paddingBottom: 62

    },
    btn: {
        width: '54%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
        padding: 30,
        backgroundColor: '#ff2882',
        borderRadius: 20
    }
})
export default Info
