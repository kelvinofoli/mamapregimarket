import React, { Component, useEffect } from 'react'
import { View, Text, StyleSheet, Button, TouchableOpacity, Dimensions, Image, ScrollView, ActivityIndicator } from 'react-native'
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
import CartContext from '../../context/cart-context';
import { useIsFocused } from "@react-navigation/native";

const { width, height } = Dimensions.get('window');
const placeholder = require('../../assets/logo_icons/logoIcons_app2.png')
class Product extends Component {
    state = {
        productInfo: {},
        quantity: 1,
        favourite: false,
        cart: false,
        imageIndex: 1
    }

    static contextType = CartContext;

    static getDerivedStateFromProps(props, state) {
        let newState = state
        newState.productInfo = props.route.params.product
        return newState
    }

    componentDidMount() {
        this.getFavourites()
        this.getCart()
        this.focusListener = this.props.navigation.addListener("focus", () => {
            this.getCart()
        });
    }
    componentWillUnmount() {
        this.focusListener();
    }

    async getFavourites() {
        await AsyncStorage.getItem("@favourites", (err, result) => {
            if (err) {
                console.log(err);
            } else {
                if (result == null) {
                    // console.log("@Favourites = EMPTY", result);
                } else {
                    const res = JSON.parse(result).value;
                    let found = res.find(el => el.id === this.state.productInfo.id)
                    if (found !== undefined) {
                        this.setState((state) => ({ favourite: !state.favourite }))
                    }
                }
            }
        });
    }
    async getCart() {
        await AsyncStorage.getItem("@Cart", (err, result) => {
            if (err) {
                console.log(err);
            } else {
                if (result == null) {
                    // console.log("@Cart = EMPTY", result);
                } else {
                    const res = JSON.parse(result).value;
                    let found = res.find(el => el.id === this.state.productInfo.id)
                    if (found === undefined) {
                        this.setState((state) => ({ cart: false }))
                    } else {
                        this.setState((state) => ({ cart: true }))
                    }
                }
            }
        });
    }




    async favouriteHandler() {
        this.setState((state) => ({ favourite: !state.favourite }))
        let _price;
        if (this.props.route.params.previous_screen) {
            _price = this.state.productInfo.price;
        } else {
            _price = this.state.productInfo.prices.price;
            _price /= Math.pow(10, 2);
            // _price += '.00';
        }
        if (this.state.favourite === false) {
            await AsyncStorage.getItem("@favourites", (err, result) => {
                const ID = this.props.route.params.product.id;
                if (err) {
                    console.log(err);
                } else {
                    if (result == null) {
                        // console.log("@Favourites = EMPTY", result);
                        AsyncStorage.setItem("@favourites", JSON.stringify({ "value": [{ ...this.state.productInfo, _price: _price }] }), (err, result) => {
                        })
                    } else {
                        let _result = JSON.parse(result).value
                        AsyncStorage.setItem("@favourites", JSON.stringify({ "value": [..._result, { ...this.state.productInfo, _price: _price }] }), (err, result) => {
                        })
                    }
                }
            })
        } else {
            await AsyncStorage.getItem("@favourites")
                .then(result => {
                    const ID = this.props.route.params.product.id;
                    let _result = JSON.parse(result).value;
                    _result.forEach((el, index, Arr) => {
                        if (el.id === ID) {
                            const new_result = _result.splice(index, 1);
                            AsyncStorage.setItem("@favourites", JSON.stringify({ "value": _result }), (err, result) => {
                                // console.log("error", err, "results", result);
                            })
                        }
                        // console.log('REMOVED');
                    })
                })
        }
    }
    async cartHandler(context) {
        this.setState((state) => ({ cart: !state.cart }))
        let _price;
        if (this.props.route.params.previous_screen) {
            _price = this.state.productInfo.price;
        } else {
            _price = this.state.productInfo.prices.price;
            _price /= Math.pow(10, 2);
            // _price += '.00';
        }
        if (this.state.cart === false) {
            await AsyncStorage.getItem("@Cart", (err, result) => {
                const ID = this.props.route.params.product.id;
                // console.log(result);
                if (err) {
                    console.log(err);
                } else {
                    if (result == null) {
                        // console.log("@Cart = EMPTY", result);
                        AsyncStorage.setItem("@Cart", JSON.stringify({ "value": [{ ...this.state.productInfo, chosen_Quantity: this.state.quantity, _price: _price }] }), (err, result) => {
                            // console.log("error", err, "result", result);
                        })
                        // console.log('Saved');
                    } else {
                        let _result = JSON.parse(result).value

                        AsyncStorage.setItem("@Cart", JSON.stringify({ "value": [..._result, { ...this.state.productInfo, chosen_Quantity: this.state.quantity, _price: _price }] }), (err, result) => {
                            // console.log("error", err, "results", result);
                        })
                        // console.log('All SAVED');
                    }
                }
            })
        } else {
            await AsyncStorage.getItem("@Cart")
                .then(result => {
                    const ID = this.props.route.params.product.id;
                    let _result = JSON.parse(result).value
                    _result.forEach((el, index, Arr) => {
                        if (el.id === ID) {
                            const new_result = _result.splice(index, 1);
                            AsyncStorage.setItem("@Cart", JSON.stringify({ "value": _result }), (err, result) => {
                                // console.log("error", err, "results", result);
                            })
                        }
                        // console.log('REMOVED');
                    })

                })
        }
        this.context.getCartItems()
    }
    addToQuantityHandler() {
        if (this.state.quantity <= this.state.productInfo.quantity_limit) {
            this.setState((state) => ({ quantity: state.quantity + 1 }))
        } else {
            this.setState((state) => ({ quantity: state.quantity + 1 }))
        }
    }

    subfromQuantityHandler() {
        if (this.state.quantity == 1) {
            return null
        } else {
            this.setState((state) => ({ quantity: state.quantity - 1 }))
        }
    }

    handleScroll(event) {
        let index = event.nativeEvent.contentOffset.x
        if (index == 0) {
            this.setState((state) => ({ imageIndex: 1 }))
            return
        } else if (index == width * 1) {
            this.setState((state) => ({ imageIndex: 2 }))
            return
        } else if (index == width * 2) {
            this.setState((state) => ({ imageIndex: 3 }))
            return
        } else if (index == width * 3) {
            this.setState((state) => ({ imageIndex: 4 }))
            return
        } else if (index == width * 4) {
            this.setState((state) => ({ imageIndex: 5 }))
            return
        } else if (index == width * 5) {
            this.setState((state) => ({ imageIndex: 6 }))
            return
        } else if (index == width * 6) {
            this.setState((state) => ({ imageIndex: 7 }))
            return
        } else {
            return
        }
        // console.log(event.nativeEvent.contentOffset.x);
        // console.log('screen width:'+ width);
    }
    render() {
        // price += '.00';
        let { product } = this.props.route.params;
        // console.log(product);
        let decription = product.short_description;
        let price;
        let imageURI;
        if (this.props.route.params.previous_screen) {
            price = product.price;
            imageURI = product.images[0].src;
            if (product.prices) {
                price = product.prices.price;
                price /= Math.pow(10, 2);
            }
        } else {
            price = product.prices.price;
            price /= Math.pow(10, 2);
            imageURI = product.images[0].thumbnail
        }

        const images = product.images.map((el, i) =>
            <Image
                key={i}
                style={styles.img}
                source={{ uri: el.src }}
                loadingIndicatorSource={<Image
                    style={styles.img}
                    source={require('../../assets/logo_icons/logoIcons_app.png')} />}
            />)


        // console.log(product.stock_status );
        // console.log(product.is_in_stock );
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.innerContainer}>
                    {/* <View style={styles.header}> */}





                    {/* </View> */}
                    <View style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20, backgroundColor: '#ccc', width: '100%', overflow: 'hidden' }}>
                        <CartContext.Consumer>
                            {context => <TouchableOpacity
                                style={[styles.cartBtn, { alignSelf: 'flex-start', justifyContent: null }]}
                                onPress={() => this.props.navigation.navigate('ShoppingCart')}>
                                <View style={styles.badge}>
                                    <Text style={{ color: 'white' }}>{context.cartItems}</Text>
                                </View>
                                <IconAnt
                                    name={'shoppingcart'}
                                    size={25}
                                    style={{ color: 'grey', alignSelf: 'center' }}
                                />
                            </TouchableOpacity>}
                        </CartContext.Consumer>

                        <ScrollView
                            horizontal
                            snapToInterval={width}
                            onScroll={this.handleScroll.bind(this)}
                            snapToAlignment='center'
                            decelerationRate="fast"
                            bounces={false}
                            showsHorizontalScrollIndicator={false}
                            style={{ width: '100%', borderRadius: 5, paddingRight: 20 }}
                        >


                            {images}

                        </ScrollView>
                        <Text style={styles.imageCount}>{this.state.imageIndex}/{images.length}</Text>
                    </View>


                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%', paddingHorizontal: 20, marginTop: 10 }}>

                        <Text style={[styles.price, { fontWeight: 'bold' }]}>{`GH₵ ${price}`}</Text>

                        <TouchableOpacity style={[styles.favourite, { backgroundColor: this.state.favourite ? "#ffeebf" : "#ccc" }]} onPress={() => this.favouriteHandler()}>
                            <Icon name="heart" size={20} color={this.state.favourite ? 'orange' : 'grey'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 20, marginTop: 10, alignItems: 'center' }}>
                        <Text style={styles.titleText}>
                            {product.name}</Text>
                        <View style={{ backgroundColor: product.is_in_stock || product.stock_status === 'instock' ? 'green' : 'black', padding: 3, borderRadius: 10, paddingHorizontal: 5 }}>
                            <Text style={{ color: 'white' }}>{product.is_in_stock || product.stock_status === 'instock' ? 'In stock' : 'Out of stock'}</Text>
                        </View>
                    </View>

                    <View style={styles.quantity}>
                        <TouchableOpacity disabled={this.state.cart ? true : false} onPress={() => { this.subfromQuantityHandler() }}>
                            <Icon name="minus-circle" size={40} color={'orange'} />
                        </TouchableOpacity>
                        <Text>  <Text style={{ fontWeight: 'bold' }}>Quantity:</Text>  {this.state.quantity}   </Text>
                        <TouchableOpacity disabled={this.state.cart ? true : false} onPress={() => { this.addToQuantityHandler() }}>
                            <Icon name="plus-circle" size={40} color={'orange'} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        onPress={() => this.cartHandler()}

                        disabled={this.state.productInfo.is_in_stock || this.state.productInfo.stock_status === 'instock' ? false : true}
                        contentContainerStyle={{ height: 200, width: "65%" }}

                        style={[styles.btn, { backgroundColor: !this.state.cart ? 'green' : 'red' }]}
                    >
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>{this.state.cart ? "Remove from cart" : "Add to cart"} </Text>
                    </TouchableOpacity>

                    <Text style={[styles.titleText,{marginLeft:'6%',marginTop: 40}]}>Description</Text>
                    {/* <Text style={{marginLeft:'6%'}}>{product.short_description.toString().splice(3,product.short_description.length-4)}</Text> */}
                    <Text style={{marginLeft:'6%',fontSize:16,color:'grey'}}>{decription.slice(3,product.short_description.length-4)}</Text>
                </View>
            </ScrollView>
        )

    }
}

const styles = StyleSheet.create({
    cartBtn: {
        zIndex: 99,
        position: 'absolute',
        borderRadius: 25,
        width: 40,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.7,
        flexDirection: 'row',
        marginLeft: 15,
        marginTop: 15

    },
    titleText: {
        color: 'orange',
        fontSize: 18,
        fontWeight: 'bold',
        marginRight: 10,
        width: 170
    }, 
    btn: {
        height: 50,
        width: "50%",
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        // opacity: 0.5
    },
    badge: {
        backgroundColor: 'red',
        width: 15,
        height: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        alignSelf: 'flex-end',
        position: 'absolute',
        marginLeft: 16,
        zIndex: 20
    },
    innerContainer: {
        paddingTop: 30,
        height:'100%',
        // paddingBottom: 30,
        // borderRadius: 20,
        backgroundColor: 'white',
        // alignItems: 'center',
        width: width
    },
    price: {
        // marginVertical: 20
        fontSize: 30
    },
    imageCount: {
        position: 'absolute',
        alignSelf: 'flex-end',
        marginTop: '80%',
        paddingRight: 15,
        color: 'grey',
        fontWeight: 'bold',
        zIndex: 100
    },
    quantity: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 10,
        marginHorizontal: 20
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        width: width,
        paddingHorizontal: 20,
        // justifyContent: 'space-between',
        marginBottom: 15
    },
    favourite: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 25,
        borderRadius: 10
    },
    container: {
        // flex: 1,
        height:'100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white'
        // paddingTop: height * 0.1
    },
    img: {
        width: width,
        height: width * 0.9,
        // borderRadius: 12,
        // marginRight: 10,
        backgroundColor: '#f2f2f2'
    }
})
// export default Product
export default function (props) {
    const isFocused = useIsFocused();
    return <Product {...props} isFocused={isFocused} />;
}

