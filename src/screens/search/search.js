import React, { useEffect, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Button,
    Dimensions,
    FlatList,
    ActivityIndicator,
    TouchableOpacity,
    Keyboard,
    Platform
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";

import ProductItem from '../../components/productItem/productItem'



// https://www.mamapregimarket.com/wp-json/wc/store/products/?search=boy



const Search = (props) => {

    const [searchString, setSearchStringState] = useState('');
    const [tempString, setTempStringState] = useState('');

    const [searchResults, setSearchResultsState] = useState([]);
    // const [pageUp, setPageup] = useState();
    const [found, setFoundState] = useState(true);
    const [page, setPageState] = useState(1);
    const [err, setErrState] = useState(false);
    const [isLoading, setisLoadingState] = useState(false);

    useEffect(() => {
        if (page > 1) {
            search();
        }
    }, [page])


    const isNetworkAvailable = async () => {
        const response = await NetInfo.fetch();

        if (response.isConnected && response.isInternetReachable) {
            // console.log("Connected");
            // this.fetchData();
            return null
        } else {
            // console.log(" not Connected");
            setErrState(true)
            setisLoadingState(false);
        }
    }



    const search = async () => {
        setisLoadingState(true);
        Keyboard.dismiss();
        setErrState(false);
        setFoundState(true);
        //CHECK INTERNET CONNECTION FIRST
        isNetworkAvailable();
        // if(tempString !== searchString){
        //     setSearchResultsState([]);
        //     setPageState(1);
        // }

        // console.log(searchString);
        const response = await fetch(`https://www.mamapregimarket.com/wp-json/wc/store/products/?search=${searchString}&page=${page}`)
            .then(response => {
                return response.json()
            })
            .then(response => {
                setisLoadingState(false);
                if (response.length == 0 && searchResults.length < 1) {
                    setFoundState(false)
                } else {
                    setFoundState(true)
                    setErrState(false)
                }
                setSearchResultsState((state) => ([...state, ...response]));
            })
            .catch(err => {
                setErrState(true)
            })

    }



    return (
        <View style={styles.container}>
            <View style={styles.corkpit}>
                <View style={styles.searchInput}>
                    <TextInput selectionColor='orange' autoCapitalize='none' style={styles.input} onChangeText={(t) => { 
                        setSearchStringState(t)
                    setSearchResultsState([])
                    setPageState(1)
                    }} on />
                    <Button title={'Search'} color={'orange'} onPress={()=> {  

                        if(searchString == ''){
                            return
                        }else{

                           return search()
                        }
                        
                    }} />
                </View>
                {/* <View style={styles.category}>
                    <Text style={{ color: 'white' }}>Category</Text>
                </View> */}
            </View>

            {!found ? <View style={{ marginTop: 10, alignSelf: 'center' }}><Text>Not Found</Text></View> : null}
            {err && isLoading == false ? <View style={{ marginTop: 50, alignSelf: 'center' }}><Text style={{ color: 'red' }}>Please check your internet connection</Text></View> : null}

            <FlatList
                data={searchResults}
                keyExtractor={(x, i) => i}
                numColumns={2}
                onEndReached={() => {
                    // this.setState((state) => ({ page: state.page + 1 }), () => { this.fetchData() }
                    setPageState((state) => state + 1);
                }}
                onEndReachedThreshold={0.5}
                ListFooterComponent={<ActivityIndicator size="large" color="orange" animating={isLoading} />}
                renderItem={itemData => {
                    // console.log(itemData);
                    // if (itemData === null) {
                    //     return <Text>Not Found</Text>
                    // }
                    return <TouchableOpacity style={styles.gridItem} onPress={() => {
                        props.navigation.navigate('Product', {
                            product: itemData.item
                        })
                    }}>
                        <ProductItem itemData={itemData.item} />
                    </TouchableOpacity>
                }
                }
            />
            {/* {isLoading ? <ActivityIndicator  color="orange" size="large" /> : null} */}

        </View>
    )
}
const styles = StyleSheet.create({
    category: {
        backgroundColor: 'orange',
        alignSelf: 'flex-start',
        marginTop: 4,
        borderRadius: 3,
        padding: 5
    },
    container: {
        paddingVertical: 62,
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        paddingTop: Platform.OS === 'android'? 62:100
        // alignItems:'center'
    },
    corkpit: {
        // width: Dimensions.get('window').width / 1.6,
        width: '100%',
        alignItems: 'center',
        marginBottom: 20,

    },
    gridItem: { flex: 1, margin: 15, alignItems: 'center' },
    searchInput: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderRadius: 5,
        borderColor: 'orange',
        padding: 0,
        justifyContent: 'space-between',
        width: Dimensions.get('window').width / 1.6,
    },
    input: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 0,
        paddingHorizontal: 7,
        width: '60%'
    }
})
export default Search
