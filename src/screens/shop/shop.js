import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    Dimensions,
    TouchableNativeFeedback,
    TouchableOpacity,
    Platform,
    ActivityIndicator
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import ProductItem from '../../components/productItem/productItem'
// import ModalDropdown from 'react-native-modal-dropdown';
import Loader from '../../components/loader';

const { width, height } = Dimensions.get('window');

const ButtonComponent = TouchableOpacity;

if (Platform.OS === 'android' && Platform.Version >= 21) {
    const ButtonComponent = TouchableNativeFeedback;
}

//ALL PRODUCTS https://mamapregimarket.com/wp-json/wc/store/products

// one PRoduct https://mamapregimarket.com/wp-json/wc/store/products/id
// {
//     title: 'Mamapregi Hospital Bag', subcar: [
//         'Mamapregi Princess',
//         'Mamapregi Prince',
//         'Mamapregi Double Joy',
//         'Mamapregi Little Hero',
//         'Mamapregi Next Round',
//     ]
// },
const categories = [
    { slugTitle: 'mamapregi-hospital-bag', title: 'Mamapregi Hospital Bag' },
    { slugTitle: 'akwaaba-gift-box', title: 'Akwaaba Gift Box' },
    { slugTitle: 'baby-footwear', title: 'Baby Footwear' },
    { slugTitle: 'feeding', title: 'Feeding' },
    { slugTitle: 'baby-hair-accessories', title: 'Baby Hair Accessories' },
    { slugTitle: 'health-beauty', title: 'Health & Beauty' },
    { slugTitle: 'toys-learning', title: 'Toys & Learning' },
    { slugTitle: 'baby-clothing', title: 'Baby Clothing' },
    { slugTitle: 'baby-changing-nappies', title: 'Baby Changing & Nappies' },
    { slugTitle: 'toddler-swimming-Accessories', title: 'Toddler Swimming Accessories' },
    { slugTitle: 'detergents', title: 'Detergents' },
    { slugTitle: 'baby-care', title: 'Baby Care' },
    { slugTitle: 'maternity-wear', title: 'Maternity Wear' }]

class Shop extends Component {

    state = {
        products: [],
        isloaded: false,
        page: 1,
        error: false,
    }


    componentDidMount() {
        this.isNetworkAvailable();
      
    }

    async isNetworkAvailable() {
        const response = await NetInfo.fetch();

        if (response.isConnected || response.isInternetReachable) {
            this.fetchData();
            // console.log("Connected");
        } else {
            this.setState({ error: true })
            // console.log(" not Connected");

        }
    }

    async fetchData() {
        // const response = await fetch(`https://www.mamapregimarket.com/wp-json/wc/store/products/?page=2`)
        const response = await fetch(`https://www.mamapregimarket.com/wp-json/wc/store/products/?page=${this.state.page}`)
            .then(response => {
                // console.log(response);
                return response.json()
            })
            .then(response => {
                // console.log(response);
                this.setState((state) => ({
                    products: [...state.products, ...response],
                    isloaded: true
                }))
            })
            .catch(err => {
                console.log(err)
                this.setState({ error: true })
            })

    }

    render() {

        // const categoriesTitles = categories.map((element) => element.title)

        const { products, isloaded } = this.state;
        // console.log(products);
        // console.log(isloaded);
        if (isloaded) {
            return (
                <SafeAreaView style={styles.container}>
                    <View style={styles.header}>
                        <Text style={{ fontSize: 19 }}>All products</Text>
                        {/* <TouchableOpacity style={styles.cartBtn}><Text style={styles.headerText}>Categories</Text></TouchableOpacity> */}
                        {/* <ModalDropdown
                            // ref="dropdown_2"
                            options={categoriesTitles}
                            defaultValue="Categories"
                            style={styles.cartBtn}
                            textStyle={{ color: 'white' }}
                            onSelect={(i, v) =>
                                this.props.navigation.navigate('CategoryScreen', {
                                    categoryID: v
                                })
                                // this.render()
                                // this.refs.dropdown_2.select(-1)
                            
                            
                            }
                        /> */}

                    </View>
                    {/* {this.state.error ? <View style={{ marginTop: 50, alignSelf: 'center' }}><Text style={{ color: 'red' }}>Please check your internet connection</Text></View> : null} */}
                    <FlatList
                        data={products}
                        keyExtractor={(x, i) => i}
                        numColumns={2}
                        onEndReached={() => {
                            this.setState((state) => ({ page: state.page + 1 }), () => { this.fetchData() })
                        }}
                        onEndReachedThreshold={5}
                        ListFooterComponent={<ActivityIndicator size="large" color="grey" />}
                        renderItem={itemData =>
                            <TouchableOpacity style={styles.gridItem} onPress={() => {
                                this.props.navigation.navigate('Product', {
                                    product: itemData.item
                                })
                            }}>
                                <ProductItem itemData={itemData.item} />
                            </TouchableOpacity>
                        }
                    />
                </SafeAreaView>
            )
        }

        return (
            <><View style={{ justifyContent: 'center', flex: 1 }}>
                {this.state.error ? <View style={{ marginTop: 50, alignSelf: 'center' }}><Text style={{ color: 'red' }}>Please check your internet connection</Text></View> : <Loader />}
            </View>
            </>
        )
    }
}
const styles = StyleSheet.create({
    gridItem: { flex: 1, margin: 15, alignItems:'center'},
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: width * 0.19,
        marginTop: height * 0.08,
        marginBottom: 10,
        marginRight: 10,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        paddingBottom: 62
    },
    categoryItem: {
        width: width * 0.65,
        marginVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        backgroundColor: 'orange',
        borderRadius: 14,
    },
    categoryText: {
        fontSize: 15,
        width: '66%',
        color: 'white',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        fontSize: 15,
        color: 'white',

    },
    cartBtn: {
        backgroundColor: 'orange',
        padding: 10,
        borderRadius: 5,

    }
})
export default Shop