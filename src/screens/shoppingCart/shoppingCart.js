import React, { useState, useEffect, useContext } from 'react'
import { View, Text, FlatList, StyleSheet, Dimensions, ImageBackground, TouchableOpacity, Button, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { useIsFocused } from "@react-navigation/native";
import AsyncStorage from '@react-native-community/async-storage';
import Modal from '../../components/modal'
import { ScrollView, TextInput } from 'react-native-gesture-handler';

import CartContext from '../../context/cart-context';

const { width, height } = Dimensions.get('window');

const CartItem = (props) => {
    let price = props.itemData._price;
    // price /= Math.pow(10, 2);
    return (<>
        <View style={styles.cartItem}>
            <ImageBackground source={{ uri: props.itemData.images[0].thumbnail||props.itemData.images[0].src }} style={{ width: 60, height: 60 }}>
                <View style={[styles.itemStatus, { backgroundColor: props.itemData.is_in_stock || props.itemData.stock_status ? 'green' : 'black' }]}>
                    <Text style={{ color: 'white', fontSize: 9 }}>{props.itemData.is_in_stock || props.itemData.stock_status === 'instock'? 'In stock' : 'Out of stock'}</Text>
                </View>
            </ImageBackground>
            <View style={{ marginLeft: 10 }}>
                <Text style={styles.itemName}>{props.itemData.name}</Text>
                <Text style={styles.price}>Quantity: {props.itemData.chosen_Quantity}</Text>
                <Text style={styles.price}>{`GH₵ ${price}`}</Text>
            </View>
            <TouchableOpacity style={styles.remove} onPress={() => { props.remove(props.itemData.id) }}>
                <Icon name="close" size={20} color={'white'} />
            </TouchableOpacity>
        </View>
    </>
    )
}


const ShoppingCart = (props) => {
    const [cartITEMS, setCartItemsState] = useState([])
    const [modalState, changeModalState] = useState(false);
    const [modalCState, changeModalCState] = useState(true);
    const [nameState, changeNameState] = useState('');
    const [emailState, changeEmailState] = useState('');
    const [phoneNumberState, changeNumberState] = useState('');
    const [totalCost, setTotalCostState] = useState(0);
    const [promoMultiplier, setpromoMultiplierState] = useState(1);
    const [cost_Discount, setcost_DiscountState] = useState(0);
    const [location, setLocationState] = useState('');
    const [isLoading, setisLoadingState] = useState();

    const [promoState, changepromoState] = useState(true);
    const [promeStatus, setpromeStatusState] = useState(<Text></Text>);
    const [promoUsed, changepromoUsedState] = useState(false);
    const [promoCode, setPromoCodeState] = useState('none');
    const isFocused = useIsFocused();

    const cartContext = useContext(CartContext)
    useEffect(() => {
        getCartItems()
    }, [])
    useEffect(() => {
        getCartItems()
    }, [isFocused])


    const SUBMIT = async () => {
        let data = {
            name: nameState,
            email: emailState,
            totalCost: totalCost,
            cost_Discount: cost_Discount,
            itemData: cartITEMS,
            phoneNumber: phoneNumberState,
            promoCode: promoCode,
            location: location
        }
        setisLoadingState(true)
        if (data.name && data.email && data.totalCost && data.itemData && data.phoneNumber && data.location) {
            fetch('https://mamapregi-server.herokuapp.com/checkout', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: JSON.stringify(data)
            })
                .then(response => response.json())
                .then(data => {
                    if (data.response == "CART RECEIVED") {
                        // console.log(data)
                        AsyncStorage.removeItem('@Cart', () => {
                            // console.log('CONTEXT runs');
                            // cartContext.getCartItems();
                        })
                        changeModalCState(false)
                        props.navigation.navigate('Home')
                        alert('Done!\nWe will get back to you shortly to confirm your order')
                        setisLoadingState(false)
                    } else {
                        setisLoadingState(false)
                        // changeModalCState(false)
                        alert('Please check you internet connection and try again')
                    }
                });
        } else {
            setisLoadingState(false)
            alert('Please fill all the required field!')
        }
        // cartContext.getCartItems();
    }

    const getCartItems = async () => {
        await AsyncStorage.getItem("@Cart", (err, result) => {
            if (err) {
                console.log(err);
            } else {
                if (result == null) {
                } else if (result === undefined) {
                    setCartItemsState([])
                } else {
                    setCartItemsState(JSON.parse(result).value)
                }
            }
        });

    }
    const removeItem = async (_ID) => {
        await AsyncStorage.getItem("@Cart")
            .then(result => {
                const ID = _ID;
                let _result = JSON.parse(result).value
                _result.forEach((el, index, Arr) => {
                    if (el.id === ID) {
                        const new_result = _result.splice(index, 1);
                        AsyncStorage.setItem("@Cart", JSON.stringify({ "value": _result }), (err, result) => {
                        })
                    }
                })

            })

        getCartItems()
        cartContext.getCartItems();
    }
    const modalFunction = () => {
        let t = 0
        cartITEMS.forEach((el) => { t = t + (el.chosen_Quantity * parseInt(el._price)) })
        // t /= Math.pow(10, 2);
        setTotalCostState(t);
        changeModalState(true);
        setTimeout(() => {
            changeModalState(false);
        }, 100)
    }

    // let discount;?

    const checkPromoCode = () => {
        if (promoUsed) {
            promoMessage(<Text style={{ color: 'red' }}>You have already{'\n'}activated a promo code</Text>)
            return
        }
        if (promoCode == 'none') {
            changepromoState(false)
            return
        }

        let data = {
            code: promoCode,
            timeUsed: Date()
        };

        fetch('https://mamapregi-server.herokuapp.com/promo', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(res => {
                // console.log(res);
                if (res == "fivePercent") {
                    let cost = totalCost;
                    cost = cost * 0.95;
                    setpromoMultiplierState(0.95)
                    setcost_DiscountState(cost)
                    changepromoUsedState(true)
                    promoMessage(<Text style={{ color: 'green' }}>5% promo code{'\n'}activated</Text>)
                    changepromoState(true)
                } else if (res == "eightPercent") {
                    let cost = totalCost;
                    cost = cost * 0.92
                    setpromoMultiplierState(0.92)
                    cost_DiscountState(cost)
                    promoMessage(<Text style={{ color: 'green' }}>5% promo code{'\n'}activated</Text>)
                    changepromoUsedState(true)
                    changepromoState(true)
                } else if (res == "tenPercent") {
                    let cost = totalCost;
                    cost = cost * 0.90;
                    setpromoMultiplierState(0.90)
                    cost_DiscountState(cost)
                    changepromoUsedState(true)
                    promoMessage(<Text style={{ color: 'green' }}>5% promo code{'\n'}activated</Text>)
                    changepromoState(true)
                } else if (res == "NOT FOUND") {
                    changepromoState(false)
                }

            })

    }

    const promoMessage = (m) => {
        setpromeStatusState(m)
    }

    if (cartITEMS.length < 1) {
        return <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}><Text style={{ color: 'orange' }}>No items</Text></View>
    } else {
        if (isLoading) {
            return <ActivityIndicator size='large' animating color="orange" style={{ marginTop: 200 }} />
        }
        return (
            <View style={styles.container}>
                {/* <View style={{backgroundColor:'red', zIndex:20, alignSelf:'center'}}> */}
                <Modal modal={modalState} modalC={modalCState}>
                    <ScrollView style={{ width: '100%', height: '100%' }} contentContainerStyle={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 17, marginBottom: 10 }}><Text style={{ fontWeight: 'bold' }}>Total cost :</Text> GH₵ {totalCost * promoMultiplier}</Text>
                        <View style={styles.wrapper}>
                            <View style={{ marginRight: 20 }}>
                                {promeStatus}
                                <View style={[styles.wrapper_, { borderColor: promoState ? 'orange' : 'red' }]}>
                                    <TextInput
                                        style={{ color: '#ff7b00' }}
                                        placeholder='Promo Code'
                                        maxLength={7}
                                        onChangeText={(val) => setPromoCodeState(val)}
                                    />
                                </View>
                                <Text style={{ fontSize: 10, color: '#8c8c8c' }}>Not required</Text>
                            </View>
                            <Button title='Enter' color='orange' contentContainerStyle={{ alignSelf: 'center', marginLeft: 20 }} onPress={checkPromoCode} />
                        </View>
                        <TextInput placeholder='Name' style={styles.input} onChangeText={(val) => changeNameState(val)} />
                        <TextInput autoCapitalize={'none'} placeholder='Email' style={styles.input} onChangeText={(val) => changeEmailState(val)} />
                        <TextInput placeholder='Location' style={styles.input} onChangeText={(val) => setLocationState(val)} />
                        <TextInput placeholder='Phone number' keyboardType='number-pad' style={[styles.input, { marginBottom: 10 }]} onChangeText={(val) => changeNumberState(val)} />
                        <Button title='submit' style={{ marginTop: 20 }} onPress={() => SUBMIT()} />
                        <View style={{ width: 100, height: 100 }}></View>
                        <View style={{ width: 100, height: 100 }}></View>
                        <View style={{ width: 100, height: 100 }}></View>
                        <View style={{ width: 100, height: 100 }}></View>
                        <View style={{ width: 100, height: 100 }}></View>
                        <View style={{ width: 100, height: 100 }}></View>
                    </ScrollView>
                </Modal>
                {/* </View> */}
                <FlatList
                    data={cartITEMS}
                    keyExtractor={(x, i) => (x.permalink)}
                    renderItem={itemData => {
                        // console.log(itemData);
                        return (
                            <TouchableOpacity style={styles.gridItem}>
                                <CartItem itemData={itemData.item} remove={removeItem} />
                            </TouchableOpacity>
                        )
                    }
                    }
                    ListFooterComponent={<View style={{ width: 100, alignSelf: 'center' }}><Button title={'Checkout'} onPress={() => modalFunction()} /></View>}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    wrapper_: {
        borderWidth: 2,
        borderRadius: 5,
        // padding: 4,
        height: 50,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    discounts: {
        justifyContent: 'space-around',
        width: '65%',
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    itemStatus: { backgroundColor: 'green', alignSelf: 'flex-end', padding: 3 },
    gridItem: { width: width, alignItems: 'center' },
    input: {
        width: '80%',
        height: 50,
        // borderRadius: 5,
        justifyContent: 'space-around',
        backgroundColor: 'white',
        paddingLeft: 7,
        paddingTop: 7,
        borderBottomWidth: 1,
        borderColor: '#ddd'

    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    img: {
        width: 130,
        height: 130
    },
    remove: {
        borderRadius: 16,
        width: 25,
        height: 25,
        backgroundColor: '#bf0000',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        margin: 30
    },
    cartItem: {
        flexDirection: 'row', borderRadius: 5, backgroundColor: '#eaeaea', alignItems: 'center', padding: 12, width: width * 0.8, marginVertical: 10,

    },
    itemName: { color: '#005296', fontSize: 12, width: 100 },

})
export default ShoppingCart
